const TOAST_MESSAGES = {
  transactions: {
    success: {
      type: 'success',
      text1: 'Trasnaction success',
    },
    failed: {
      type: 'error',
      text1: 'Trasnaction failed',
    },
    notEnoughMoney: {
      type: 'error',
      text1: 'You don`t have enough money',
    },
  },
  validation: {
    onlyDigits: {
      type: 'error',
      text1: 'Please enter only digits',
    },
  },
};

const WALLET_ACTION_TYPES = {
  BUY: 'buy',
  SELL: 'sell',
};


const REQUEST_TIMES = {
  REQUEST_DELAY :2000,
  INTERVAL_CONVERSTION_RATES:30000
}


export {
  TOAST_MESSAGES,
  WALLET_ACTION_TYPES,
  REQUEST_TIMES
};
