export default {
    black_opacity:'rgba(0,0,0,0.5)',
    white:'#ffffff',
    purple:'#181a2d',
    purple_a:'#1f223c',
    purple_b:'#262949',
    purple_c:'#696d9a',
    black:'#020203',
    blue:'#56b2f8',
    grey:'#e8e8e8'
}