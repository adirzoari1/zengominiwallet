import { REQUEST_TIMES} from './constants'
const cryptoList = [
  {
    currencyName: 'Bitcoin',
    currencySymbol: 'BTC',
    currencyImage: require('../assets/images/btc.png'),
  },
  {
    currencyName: 'Ethereum',
    currencySymbol: 'ETH',
    currencyImage: require('../assets/images/eth.png'),
  },
  {
    currencyName: 'Litecoin',
    currencySymbol: 'LTC',
    currencyImage: require('../assets/images/ltc.png'),
  },
  {
    currencyName: 'Neo',
    currencySymbol: 'NEO',
    currencyImage: require('../assets/images/neo.png'),
  },
  {
    currencyName: 'Cosmom',
    currencySymbol: 'ATOM',
    currencyImage: require('../assets/images/atom.png'),
  },
];

function getCryptoDataMock() {
  return cryptoList.map((c) => {
    return {
      ...c,
      balance: 0,
      balanceRated: 0,
    };
  });
}

function getCryptoData(conversionRate) {
  const data = getCryptoDataMock(conversionRate);
  return fetchData(data);
}

function getCryptoConversionRatesMockData() {
  const conversionRates = {};
  cryptoList.forEach((c) => {
    conversionRates[c.currencySymbol] = getRandomNumber();
  });
  return conversionRates;
}

function getRandomNumber() {
  return Math.pow(2, Math.floor(Math.random() * 4 + 1));
}
function getCryptoConversionRates() {
  const data = getCryptoConversionRatesMockData();
  return fetchData(data);
}

function getTransactionStatus() {
  const transactionStatus = Math.random() < 0.5;
  return fetchData(transactionStatus);
}

function fetchData(data) {
  return new Promise((resolve) => setTimeout(() => resolve(data), REQUEST_TIMES.REQUEST_DELAY));
}

export {
  getCryptoDataMock,
  getCryptoData,
  getCryptoConversionRates,
  getTransactionStatus,
};
