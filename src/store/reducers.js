import { combineReducers } from 'redux'

import cryptoWalletReducer from './cryptoWallet/cryptoWallet.reducer';
import appReducer from './app/app.reducer'
const reducers = combineReducers({
  cryptoWallet: cryptoWalletReducer,
  app:appReducer,
});


export default reducers;