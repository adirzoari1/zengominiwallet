import {
  GET_MY_CRYTPO_WALLET_SUCCESS,
  GET_CRYPTO_CONVERSION_RATES_SUCCESS,
  TRANSACTION_BUY_COINS_SUCCESS,
  TRANSACTION_BUY_COINS_FAILED,
  TRANSACTION_SELL_COINS_SUCCESS,
  TRANSACTION_SELL_COINS_FAILED,
} from './cryptoWallet.types';
import {REQUEST_START, REQUEST_FINISH} from '../app/app.types';
import {
  getCryptoConversionRates,
  getCryptoData,
  getTransactionStatus,
} from '../../utils/mockData';

export function getMyCryptoWalletAction() {
  return (dispatch) => {
    dispatch({type: REQUEST_START});
    return getCryptoConversionRates().then((conversionRate) => {
      return getCryptoData(conversionRate).then((data) => {
        dispatch({type: GET_MY_CRYTPO_WALLET_SUCCESS, payload: data});
        dispatch({type: REQUEST_FINISH});
      });
    });
  };
}

export function getCryptoConversionRatesAction() {
  return (dispatch) => {
    dispatch({type: REQUEST_START});
    return getCryptoConversionRates().then((conversionRates) => {
      dispatch({
        type: GET_CRYPTO_CONVERSION_RATES_SUCCESS,
        payload: conversionRates,
      });
      dispatch({type: REQUEST_FINISH});
    });
  };
}

export function buyCoinsAction(data) {
  return (dispatch) => {
    dispatch({type: REQUEST_START});
    return getTransactionStatus().then((approved) => {
      approved
        ? dispatch({type: TRANSACTION_BUY_COINS_SUCCESS, payload: {data}})
        : dispatch({type: TRANSACTION_BUY_COINS_FAILED});

      dispatch({type: REQUEST_FINISH});

      return approved;
    });
  };
}

export function sellCoinsAction(data) {
  return (dispatch) => {
    dispatch({type: REQUEST_START});
    return getTransactionStatus().then((approved) => {
      approved
        ? dispatch({type: TRANSACTION_SELL_COINS_SUCCESS, payload: {data}})
        : dispatch({type: TRANSACTION_SELL_COINS_FAILED});

        dispatch({type: REQUEST_FINISH});

      return approved;
    });
  };
}
