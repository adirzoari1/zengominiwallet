import {createSelector} from 'reselect';
const baseCryptoWallet = (state) => state.cryptoWallet;

const cryptoCurrenciesDataSelector = createSelector(
  baseCryptoWallet,
  (cryptoWallet) => cryptoWallet.data,
);

const cryptoCurrenciesListSelector = createSelector(
  cryptoCurrenciesDataSelector,
  (cryptoData) => {
    return cryptoData.map((c) => {
      return {
        ...c,
        balance:parseFloat(c.balance.toFixed(2)),
        balanceRated:parseFloat(c.balanceRated.toFixed(2)),
      };
    });
  },
);

const cryptoCurrentSelectedSelector = (id) =>
  createSelector(
    cryptoCurrenciesListSelector,
    (cryptoData) => cryptoData.filter((c) => c.currencySymbol === id)[0],
  );

export {
  cryptoCurrenciesListSelector,
  cryptoCurrentSelectedSelector,
};
