import {
  GET_MY_CRYTPO_WALLET_SUCCESS,
  GET_CRYPTO_CONVERSION_RATES_SUCCESS,
  TRANSACTION_BUY_COINS_SUCCESS,
  TRANSACTION_SELL_COINS_SUCCESS,
} from './cryptoWallet.types';

import {getCryptoDataMock} from '../../utils/mockData';
const initialState = {
  isLoading: false,
  data: getCryptoDataMock(),
  conversionRates: {},
};

const cryptoWalletReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MY_CRYTPO_WALLET_SUCCESS:
      return {
        ...state,
        data: action.payload,
      };

    case GET_CRYPTO_CONVERSION_RATES_SUCCESS:
      return {
        ...state,
        conversionRates: action.payload,
        data: state.data.map((c) => {
          return {
            ...c,
            balanceRated: c.balance * action.payload[c.currencySymbol],
          };
        }),
      };
    case TRANSACTION_BUY_COINS_SUCCESS: {
      const {id, amount} = action.payload.data;
      return {
        ...state,
        data: state.data.map((c) => {
          if (c.currencySymbol == id) {
            const totalMoney = c.balanceRated + amount;
            const increasedBalance = amount / state.conversionRates[id];
            return {
              ...c,
              balance: c.balance + increasedBalance,
              balanceRated: totalMoney,
            };
          }
          return c;
        }),
      };
    }

    case TRANSACTION_SELL_COINS_SUCCESS: {
      const {id, amount} = action.payload.data;
      return {
        ...state,
        data: state.data.map((c) => {
          if (c.currencySymbol == id && c.balanceRated - amount >= 0) {
            const totalMoney = c.balanceRated - amount;
            const increasedBalance = amount / state.conversionRates[id];
            return {
              ...c,
              balance: c.balance - increasedBalance,
              balanceRated: totalMoney,
            };
          }
          return c;
        }),
      };
    }
    default: {
      return state;
    }
  }
};

export default cryptoWalletReducer;
