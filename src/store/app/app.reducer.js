import {REQUEST_START, REQUEST_FINISH} from './app.types';
const initialState = {
  isLoading: false,
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_START:
      return {
        ...state,
        isLoading: true,
      };
    case REQUEST_FINISH:
      return {
        ...state,
        isLoading: false,
      };

    default: {
      return state;
    }
  }
};

export default appReducer;
