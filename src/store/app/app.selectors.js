import {createSelector} from 'reselect';
const baseApp = (state) => state.app;

const loadingSelector = createSelector(
  baseApp,
  (app) => app.isLoading,
);


export {
  loadingSelector,
};
