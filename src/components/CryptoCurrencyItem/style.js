import {StyleSheet} from 'react-native';
import Colors from '../../utils/colors';
const style = StyleSheet.create({
  containerCrtyptoItem: {
    width: '100%',
    height: 200,
    borderRadius: 10,
    justifyContent: 'center',
    padding: 30,
    alignSelf: 'center',
    marginVertical: 10,
    backgroundColor: Colors.purple_b,
   
  },
  viewCryptoImage: {},
  imageCrypto: {
    width: 50,
    height: 50,
  },
  viewCurrencyName: {
    marginTop: 20,
  },
  textCryptoCurrencyTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.white,
  },
  textCryptoCurrencySymbol: {
    fontSize: 12,
    color: Colors.grey,
  },
  viewTextCurrency: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '50%',
    marginTop: 10,
  },
  textTitleCurrency: {
    fontSize: 15,
    color: Colors.grey,
  },
  textCurrencyValue: {
    fontSize: 18,
    fontWeight: 'bold',
    color: Colors.white,
  },
  viewDots: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 20,
    right: 20,
  },
  viewDot: {
    backgroundColor: Colors.purple_c,
    opacity: 0.7,
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
  },
  viewDotB: {
    backgroundColor: Colors.purple_a,
    opacity: 0.3,
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
  },
  btnShowDetails: {
    zIndex: 9999,
    position: 'absolute',
    top: 5,
    right: 10,
    backgroundColor: Colors.blue,
    width: 100,
    height: 30,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textShowDetails: {
    fontSize: 13,
    color: Colors.white,
  },
});

export default style;
