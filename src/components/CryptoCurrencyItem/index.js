import React from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';

import styles from './style';
function CryptoCurrencyItem({
  currencyName,
  currencySymbol,
  currencyImage,
  balance,
  balanceRated,
  onPressItem,
}) {
  return (
    <TouchableOpacity onPress={onPressItem}>
      <View style={styles.containerCrtyptoItem}>
        <View style={styles.viewCryptoImage}>
          <Image style={styles.imageCrypto} source={currencyImage} />
        </View>

        <View style={styles.viewCurrencyName}>
          <Text style={styles.textCryptoCurrencyTitle}>{currencyName}</Text>
        </View>

        <View style={styles.viewTextCurrency}>
          <Text style={styles.textTitleCurrency}> balance:</Text>
          <Text style={styles.textCurrencyValue} numberOfLines={1}>
            {balance} {currencySymbol}
          </Text>
        </View>

        <View style={styles.viewTextCurrency}>
          <Text style={styles.textTitleCurrency}> balance $:</Text>
          <Text style={styles.textCurrencyValue} numberOfLines={1}>
            {balanceRated}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default CryptoCurrencyItem;
