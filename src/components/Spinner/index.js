import React from 'react';
import {ActivityIndicator, View, Text} from 'react-native';
import {useSelector} from 'react-redux';

import styles from './style';
import {loadingSelector} from '../../store/app/app.selectors';
function Spinner({textSpin = 'Loading...'}) {
  const isLoading = useSelector(loadingSelector);
  return isLoading ? (
    <View style={styles.container}>
      <View style={styles.wrapperSpinner}>
        <ActivityIndicator size="small" />
        <Text style={styles.textSpinner}>{textSpin}</Text>
      </View>
    </View>
  ) : null;
}

export default Spinner;
