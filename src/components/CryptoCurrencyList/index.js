import React from 'react';
import {FlatList} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {useSelector} from 'react-redux';
import CryptoCurrencyItem from '../CryptoCurrencyItem';
import styles from './style';
import {cryptoCurrenciesListSelector} from '../../store/cryptoWallet/cryptoWallet.selectors';
function CryptoCurrencyList() {
  const cryptoCurrenciesList = useSelector(cryptoCurrenciesListSelector);
  const navigation = useNavigation();

  function renderCryptoCurrencyItem({item, index, separators}) {
    return (
      <CryptoCurrencyItem
        {...item}
        key={index}
        onPressItem={() =>
          navigation.navigate('Details', {id: item.currencySymbol})
        }
      />
    );
  }
  return (
    <FlatList
      data={cryptoCurrenciesList}
      renderItem={renderCryptoCurrencyItem}
      showsVerticalScrollIndicator={false}
      keyExtractor={(item) => item.currencySymbol}
      contentContainerStyle={styles.listContentContainer}
    />
  );
}

export default CryptoCurrencyList;
