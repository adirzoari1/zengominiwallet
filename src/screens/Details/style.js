import {StyleSheet} from 'react-native';
import Colors from '../../utils/colors';
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.purple,
    padding: 10,
    justifyContent: 'center',
  },
  wrapperContent: {
    flex: 1,
    borderRadius: 50,
    alignItems: 'center',
    padding: 40,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentButton: {
    flex: 1,
    justifyContent: 'flex-end',
    width: '100%',
  },
  wrapperButtons: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  viewCryptoImage: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageCrypto: {
    width: 80,
    height: 80,
  },

  viewHeaderTitle: {
    marginVertical: 20,
    alignItems: 'center',
  },
  textHeadeTitle: {
    fontSize: 40,
    fontWeight: 'bold',
    color: Colors.purple_c,
  },
  wrapperCurrencyDisplay:{
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewTextCurrency: {
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
    margin:20,


  },
  textTitleCurrency: {
    fontSize: 12,
    color: Colors.grey,
  },
  textCurrencyValue: {
    fontSize: 25,
    fontWeight: 'bold',
    color: Colors.white,
  },
  wrapperBtn: {
    backgroundColor: Colors.blue,
    width: 100,
    height: 40,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBtn: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.white,
  },
});

export default style;
