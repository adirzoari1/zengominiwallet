import React, {useEffect, useState} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import DialogInput from 'react-native-dialog-input';
import Toast from 'react-native-toast-message';
import styles from './style';
import {
  buyCoinsAction,
  sellCoinsAction,
} from '../../store/cryptoWallet/cryptoWallet.actions';
import {cryptoCurrentSelectedSelector} from '../../store/cryptoWallet/cryptoWallet.selectors';
import {TOAST_MESSAGES, WALLET_ACTION_TYPES} from '../../utils/constants';

function Details({route, navigation}) {
  const {id} = route.params;
  const crtyptoSelected = useSelector(cryptoCurrentSelectedSelector(id));
  const dispatch = useDispatch();

  const [isDialogVisible, setDialogVisible] = useState(false);
  const [dialogProperties, setDialogProperties] = useState({
    title: '',
    message: '',
    type: '',
  });

  useEffect(() => {
    navigation.setOptions({
      title: `${crtyptoSelected.currencyName} (${crtyptoSelected.currencySymbol})`,
    });
    return () => Toast.hide();
  }, []);

  function buyCoins(amount) {
    dispatch(buyCoinsAction({id: crtyptoSelected.currencySymbol, amount})).then(
      handlingTransactionResponse,
    );
  }

  function sellCoins(amount) {
    const {balanceRated} = crtyptoSelected;
 

    if (balanceRated && balanceRated - amount >= 0) {
      dispatch(
        sellCoinsAction({id: crtyptoSelected.currencySymbol, amount}),
      ).then(handlingTransactionResponse);
    } else {
      showToast(TOAST_MESSAGES.transactions.notEnoughMoney);
    }
  }

  function showToast(options) {
    Toast.show({...options, bottomOffset: 140, position: 'bottom'});
  }

  function handlingTransactionResponse(success) {
    if (success) {
      showToast(TOAST_MESSAGES.transactions.success);
    } else {
      showToast(TOAST_MESSAGES.transactions.failed);
    }
  }

  function submitDialog(amount) {
    if (!isOnlyDigits(amount)) {
      showToast(TOAST_MESSAGES.validation.onlyDigits);
      return;
    }
    switch (dialogProperties.type) {
      case WALLET_ACTION_TYPES.BUY: {
        buyCoins(+amount);
        break;
      }

      case WALLET_ACTION_TYPES.SELL: {
        sellCoins(+amount);
        break;
      }
      default:
        break;
    }

    closeDialog();
  }

  function isOnlyDigits(amount) {
    return /^\d*\.?\d+$/.test(amount);
  }
  function closeDialog() {
    setDialogVisible(false);
  }

  function setDialogValue(type) {
    setDialogProperties({
      title: `${type} ${crtyptoSelected.currencyName}`,
      message: `Please enter amount of ${crtyptoSelected.currencyName} you want to ${type}`,
      type,
    });
    setDialogVisible(true);
  }

  function renderCurrencyDetails() {
    return (
      <View style={styles.content}>
        <View style={styles.viewCryptoImage}>
          <Image
            source={crtyptoSelected.currencyImage}
            style={styles.imageCrypto}
          />
        </View>
        <View style={styles.viewHeaderTitle}>
          <Text style={styles.textHeadeTitle}>
            {crtyptoSelected.currencyName}
          </Text>
        </View>
        <View style={styles.wrapperCurrencyDisplay}>
          <View style={styles.viewTextCurrency}>
            <Text style={styles.textTitleCurrency}> balance:</Text>
            <Text style={styles.textCurrencyValue} numberOfLines={1}>
              {crtyptoSelected.balance} {crtyptoSelected.currencySymbol}
            </Text>
          </View>

          <View style={styles.viewTextCurrency}>
            <Text style={styles.textTitleCurrency}> balance $:</Text>
            <Text style={styles.textCurrencyValue}>
              {crtyptoSelected.balanceRated}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderTradingActions() {
    return (
      <View style={styles.contentButton}>
        <View style={styles.wrapperButtons}>
          <TouchableOpacity
            onPress={() => {
              setDialogValue(WALLET_ACTION_TYPES.BUY);
            }}>
            <View style={styles.wrapperBtn}>
              <Text style={styles.textBtn}>Buy</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              setDialogValue(WALLET_ACTION_TYPES.SELL);
            }}>
            <View style={styles.wrapperBtn}>
              <Text style={styles.textBtn}>Sell</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <DialogInput
        isDialogVisible={isDialogVisible}
        title={dialogProperties.title}
        message={dialogProperties.message}
        hintInput={''}
        submitInput={submitDialog}
        closeDialog={closeDialog}
        textInputProps={{
          keyboardType: 'number-pad',
        }}
      />
      <Toast ref={(ref) => Toast.setRef(ref)} />

      <View style={styles.wrapperContent}>
        {renderCurrencyDetails()}
        {renderTradingActions()}
      </View>
    </View>
  );
}

export default Details;
