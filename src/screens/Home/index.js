import React, {useEffect, useState, useRef} from 'react';
import {View, Text, AppState} from 'react-native';
import {useDispatch} from 'react-redux';
import styles from './style';
import {getCryptoConversionRatesAction} from '../../store/cryptoWallet/cryptoWallet.actions';
import CryptoCurrencyList from '../../components/CryptoCurrencyList';
import {REQUEST_TIMES} from '../../utils/constants';
function Home() {
  const dispatch = useDispatch();
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  useEffect(() => {
    AppState.addEventListener('change', _handleAppStateChange);
    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  useEffect(() => {
    if (appStateVisible == 'active') {
      getCryptoConversionRatesData();
      this.intervalID = setInterval(() => {
        getCryptoConversionRatesData();
      }, REQUEST_TIMES.INTERVAL_CONVERSTION_RATES);
    }
  }, [appStateVisible]);

  function getCryptoConversionRatesData() {
    dispatch(getCryptoConversionRatesAction());
  }
  const _handleAppStateChange = (nextAppState) => {
    clearInterval(this.intervalID);
    appState.current = nextAppState;
    setAppStateVisible(appState.current);
  };

  return (
    <View style={styles.container}>
      <View style={styles.viewHeaderTitle}>
        <Text style={styles.textHeadeTitle}>My wallet</Text>
      </View>
      <CryptoCurrencyList />
    </View>
  );
}

export default Home;
