import { StyleSheet } from 'react-native'
import Colors from '../../utils/colors'
const style = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor:Colors.purple,
      padding:25,
      justifyContent:'center',
    
  },
  viewHeaderTitle:{
    marginTop:40,
    marginBottom:20,

  },
  textHeadeTitle:{
    fontSize: 40,
    fontWeight: 'bold',
    color:Colors.white,
  },
  
})

export default style
