import React from 'react'

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Home, Details} from '../screens'
import Colors from '../utils/colors';

const Stack = createStackNavigator();
export const RootNavigator = ()=>{
    return (
        <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen
            name="Home"
            component={Home}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Details"
            component={Details}
            options={{

              headerTintColor:Colors.white,
              headerStyle: {
                backgroundColor: Colors.purple,
                shadowColor: 'transparent'
              },
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    )
}

export default RootNavigator
