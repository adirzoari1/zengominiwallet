import React  from 'react';
import {View} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

import {store, persistor} from './src/store';

import RootNavigator from './src/routes';
import {Spinner} from './src/components';
function App() {
  
  return (
    <Provider store={store}>
      <PersistGate loading={<Spinner/>} persistor={persistor}>
        <View style={{flex: 1}}>
          <Spinner />
          <RootNavigator />
        </View>
      </PersistGate>
    </Provider>
  );
}

export default App;
